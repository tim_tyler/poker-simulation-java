Dec 06, 2014 6:20:15 PM poker.PokerManager <init>
INFO: Initialised Log.txt file
Dec 06, 2014 6:20:16 PM poker.GFX.GFX_SetScene setBankOnStartup
INFO: Player bank loaded
Dec 06, 2014 6:20:18 PM poker.PokerManager <init>
INFO: BGM initialised.
Dec 06, 2014 6:20:18 PM poker.PokerManager <init>
INFO: PokerManager initialised... Starting new Poker game session.
Dec 06, 2014 6:20:18 PM poker.Dealer.DeckOfCards <init>
INFO: Seed = -8,749,579,192,211,302,208
Dec 06, 2014 6:20:18 PM poker.PokerManager setGameID
INFO: GameID = 1712
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P1Bank = 2183
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P2Bank = 2849
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P3Bank = 1050
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P4Bank = 449
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P5Bank = 3650
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P6Bank = -2350
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P7Bank = 5316
Dec 06, 2014 6:20:18 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P8Bank = -2750
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: PHASE: Starting new Poker hand.
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 1 has the ACE of DIAMONDS & the ACE of CLUBS
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 2 has the 8 of HEARTS & the 7 of HEARTS
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 3 has the 5 of DIAMONDS & the ACE of SPADES
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 4 has the 10 of DIAMONDS & the 4 of DIAMONDS
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 5 has the 10 of CLUBS & the 9 of SPADES
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 6 has the 2 of SPADES & the 8 of DIAMONDS
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 7 has the ACE of HEARTS & the 3 of SPADES
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: Player 8 has the QUEEN of HEARTS & the 10 of SPADES
Dec 06, 2014 6:20:18 PM poker.PokerManager startGame
INFO: The community cards are the JACK of HEARTS, the 9 of CLUBS, the 7 of DIAMONDS, the 6 of HEARTS & the 7 of CLUBS
Dec 06, 2014 6:20:26 PM poker.PokerManager preFlop
INFO: PHASE: Pre-flop.
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2133
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 2799
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 1000
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 399
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3600
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2400
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5266
Dec 06, 2014 6:20:26 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2800
Dec 06, 2014 6:20:32 PM poker.PokerManager flop
INFO: PHASE: The Flop.
Dec 06, 2014 6:20:35 PM poker.PokerManager turn
INFO: PHASE: The Turn card.
Dec 06, 2014 6:20:36 PM poker.PokerManager river
INFO: PHASE: The River card.
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 1 has TWOPAIR with ACE of DIAMONDS, ACE of CLUBS, 7 of DIAMONDS, 7 of CLUBS, JACK of HEARTS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 2 has TRIP with 7 of HEARTS, 7 of DIAMONDS, 7 of CLUBS, JACK of HEARTS, 9 of CLUBS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 3 has PAIR with 7 of DIAMONDS, 7 of CLUBS, ACE of SPADES, JACK of HEARTS, 9 of CLUBS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 4 has PAIR with 7 of DIAMONDS, 7 of CLUBS, JACK of HEARTS, 10 of DIAMONDS, 9 of CLUBS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 5 has TWOPAIR with 9 of SPADES, 9 of CLUBS, 7 of DIAMONDS, 7 of CLUBS, JACK of HEARTS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 6 has PAIR with 7 of DIAMONDS, 7 of CLUBS, JACK of HEARTS, 9 of CLUBS, 8 of DIAMONDS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 7 has PAIR with 7 of DIAMONDS, 7 of CLUBS, ACE of HEARTS, JACK of HEARTS, 9 of CLUBS, 
Dec 06, 2014 6:20:36 PM poker.HandCalculator printBestPlayerHand
INFO: Player 8 has PAIR with 7 of DIAMONDS, 7 of CLUBS, QUEEN of HEARTS, JACK of HEARTS, 10 of SPADES, 
Dec 06, 2014 6:20:42 PM poker.PokerManager onYourBacks
INFO: PHASE: Result... Analysing winners.
Dec 06, 2014 6:20:42 PM poker.PokerManager declareWinner
INFO: Player 2 takes this hand with a 7 of HEARTS high TRIP!
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2133
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3199
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 1000
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 399
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3600
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2400
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5266
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2800
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2133
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3199
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 1000
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 399
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3600
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2400
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5266
Dec 06, 2014 6:20:42 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2800
Dec 06, 2014 6:20:43 PM poker.Dealer.DeckOfCards <init>
INFO: Seed = 527,537,325,199,357,152
Dec 06, 2014 6:20:43 PM poker.PokerManager setGameID
INFO: GameID = 1713
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P1Bank = 2133
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P2Bank = 3199
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P3Bank = 1000
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P4Bank = 399
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P5Bank = 3600
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P6Bank = -2400
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P7Bank = 5266
Dec 06, 2014 6:20:43 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P8Bank = -2800
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: PHASE: Starting new Poker hand.
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 1 has the QUEEN of SPADES & the 8 of HEARTS
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 2 has the ACE of CLUBS & the JACK of SPADES
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 3 has the 4 of HEARTS & the 10 of CLUBS
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 4 has the 3 of CLUBS & the 10 of HEARTS
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 5 has the KING of CLUBS & the 9 of DIAMONDS
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 6 has the 4 of DIAMONDS & the KING of HEARTS
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 7 has the 2 of SPADES & the 5 of SPADES
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: Player 8 has the ACE of SPADES & the 2 of DIAMONDS
Dec 06, 2014 6:20:43 PM poker.PokerManager startGame
INFO: The community cards are the ACE of DIAMONDS, the 9 of CLUBS, the 5 of DIAMONDS, the 10 of DIAMONDS & the 7 of DIAMONDS
Dec 06, 2014 6:20:45 PM poker.PokerManager preFlop
INFO: PHASE: Pre-flop.
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2083
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3149
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 950
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 349
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3550
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2450
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5216
Dec 06, 2014 6:20:45 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2850
Dec 06, 2014 6:20:52 PM poker.PokerManager flop
INFO: PHASE: The Flop.
Dec 06, 2014 6:20:57 PM poker.PokerManager turn
INFO: PHASE: The Turn card.
Dec 06, 2014 6:20:58 PM poker.PokerManager river
INFO: PHASE: The River card.
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 1 has HIGHCARD with ACE of DIAMONDS, QUEEN of SPADES, 10 of DIAMONDS, 9 of CLUBS, 8 of HEARTS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 2 has PAIR with ACE of CLUBS, ACE of DIAMONDS, JACK of SPADES, 10 of DIAMONDS, 9 of CLUBS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 3 has PAIR with 10 of CLUBS, 10 of DIAMONDS, ACE of DIAMONDS, 9 of CLUBS, 7 of DIAMONDS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 4 has PAIR with 10 of HEARTS, 10 of DIAMONDS, ACE of DIAMONDS, 9 of CLUBS, 7 of DIAMONDS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 5 has FLUSH with ACE of DIAMONDS, 10 of DIAMONDS, 9 of DIAMONDS, 7 of DIAMONDS, 5 of DIAMONDS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 6 has FLUSH with ACE of DIAMONDS, 10 of DIAMONDS, 7 of DIAMONDS, 5 of DIAMONDS, 4 of DIAMONDS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 7 has PAIR with 5 of SPADES, 5 of DIAMONDS, ACE of DIAMONDS, 10 of DIAMONDS, 9 of CLUBS, 
Dec 06, 2014 6:20:58 PM poker.HandCalculator printBestPlayerHand
INFO: Player 8 has FLUSH with ACE of DIAMONDS, 10 of DIAMONDS, 7 of DIAMONDS, 5 of DIAMONDS, 2 of DIAMONDS, 
Dec 06, 2014 6:21:03 PM poker.PokerManager onYourBacks
INFO: PHASE: Result... Analysing winners.
Dec 06, 2014 6:21:03 PM poker.PokerManager declareWinner
INFO: Player 5 takes this hand with a ACE of DIAMONDS high FLUSH!
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2083
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3149
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 950
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 349
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3950
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2450
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5216
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2850
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2083
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3149
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 950
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 349
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3950
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2450
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5216
Dec 06, 2014 6:21:03 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2850
Dec 06, 2014 6:21:07 PM poker.Dealer.DeckOfCards <init>
INFO: Seed = 6,593,412,875,886,204,436
Dec 06, 2014 6:21:07 PM poker.PokerManager setGameID
INFO: GameID = 1714
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P1Bank = 2083
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P2Bank = 3149
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P3Bank = 950
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P4Bank = 349
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P5Bank = 3950
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P6Bank = -2450
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P7Bank = 5216
Dec 06, 2014 6:21:07 PM poker.Cashier loadAndSetPlayerBankValueFromFile
INFO: P8Bank = -2850
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: PHASE: Starting new Poker hand.
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 1 has the QUEEN of HEARTS & the 6 of DIAMONDS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 2 has the 7 of HEARTS & the JACK of HEARTS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 3 has the KING of CLUBS & the 5 of SPADES
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 4 has the 5 of DIAMONDS & the 7 of DIAMONDS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 5 has the 4 of SPADES & the 4 of DIAMONDS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 6 has the ACE of SPADES & the 4 of CLUBS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 7 has the 3 of SPADES & the 2 of HEARTS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: Player 8 has the 3 of DIAMONDS & the 10 of CLUBS
Dec 06, 2014 6:21:07 PM poker.PokerManager startGame
INFO: The community cards are the KING of HEARTS, the JACK of SPADES, the 5 of HEARTS, the QUEEN of DIAMONDS & the 2 of SPADES
Dec 06, 2014 6:21:08 PM poker.PokerManager preFlop
INFO: PHASE: Pre-flop.
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2033
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3099
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 900
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 299
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3900
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2500
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5166
Dec 06, 2014 6:21:08 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2900
Dec 06, 2014 6:21:09 PM poker.PokerManager flop
INFO: PHASE: The Flop.
Dec 06, 2014 6:21:10 PM poker.PokerManager turn
INFO: PHASE: The Turn card.
Dec 06, 2014 6:21:11 PM poker.PokerManager river
INFO: PHASE: The River card.
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 1 has PAIR with QUEEN of HEARTS, QUEEN of DIAMONDS, KING of HEARTS, JACK of SPADES, 6 of DIAMONDS, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 2 has PAIR with JACK of HEARTS, JACK of SPADES, KING of HEARTS, QUEEN of DIAMONDS, 7 of HEARTS, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 3 has TWOPAIR with KING of CLUBS, KING of HEARTS, 5 of SPADES, 5 of HEARTS, QUEEN of DIAMONDS, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 4 has PAIR with 5 of DIAMONDS, 5 of HEARTS, KING of HEARTS, QUEEN of DIAMONDS, JACK of SPADES, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 5 has PAIR with 4 of SPADES, 4 of DIAMONDS, KING of HEARTS, QUEEN of DIAMONDS, JACK of SPADES, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 6 has HIGHCARD with ACE of SPADES, KING of HEARTS, QUEEN of DIAMONDS, JACK of SPADES, 5 of HEARTS, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 7 has PAIR with 2 of HEARTS, 2 of SPADES, KING of HEARTS, QUEEN of DIAMONDS, JACK of SPADES, 
Dec 06, 2014 6:21:11 PM poker.HandCalculator printBestPlayerHand
INFO: Player 8 has HIGHCARD with KING of HEARTS, QUEEN of DIAMONDS, JACK of SPADES, 10 of CLUBS, 5 of HEARTS, 
Dec 06, 2014 6:21:13 PM poker.PokerManager onYourBacks
INFO: PHASE: Result... Analysing winners.
Dec 06, 2014 6:21:13 PM poker.PokerManager declareWinner
INFO: Player 3 takes this hand with a KING of CLUBS high TWOPAIR!
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2033
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3099
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 1300
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 299
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3900
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2500
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5166
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2900
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P1Bank = 2033
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P2Bank = 3099
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P3Bank = 1300
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P4Bank = 299
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P5Bank = 3900
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P6Bank = -2500
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P7Bank = 5166
Dec 06, 2014 6:21:13 PM poker.Cashier savePlayerBankToFile
INFO: P8Bank = -2900
Dec 06, 2014 6:21:20 PM poker.PokerManager$2 windowClosing
INFO: User closed the Poker window... Exiting app.
