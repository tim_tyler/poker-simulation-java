/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import java.io.IOException;
import java.util.logging.*;

/**
 * Objectives:
 * 1. Write exceptions to log file.
 * 2. Log phase progression info.
 * 3. Log hand info.
 * 4. Log result info.
 * 
 * @author Tim
 */
public class LoggingManager {

    // use the classname for the logger, this way you can refactor
    public final static Logger PhaseInfo = Logger.getLogger(LoggingManager.class
            .getName());
    public FileHandler fh;
    
    public LoggingManager(){
        try{
            fh = new FileHandler("C:\\Users\\Tim\\Desktop\\Java\\Projects\\Poker-Dev\\Logs\\log.txt");
        } catch (IOException e) {
            PhaseInfo.log(Level.WARNING, "The log file was not loaded");
        }
        PhaseInfo.addHandler(fh);
        PhaseInfo.info("Initialised Log");
    }
}

//public class Formatter 
    

    
