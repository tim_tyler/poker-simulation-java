/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package poker;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import poker.Dealer.Hand;
import poker.PokerManager.*;

/**
 * Stores dealer and bank information.
 * @author Tim
 */
public class Cashier {
    
    // Player that is dealer, starts at P1 (incremented at start of session).
    private static int dealer = 7; 
    private static int sBlind = 0; // Player that is small blind, starts at P2
    private static int bBlind = 1; // Player that is small blind, starts at P3    
    private static final int blind = 50; // Amount of big blind. Small blind is /2.
    private static int pot = 0; // Total money in pot (value).    
    private static int raises = 0;
    private static Hand[] whoIsStillIn;

    // Returns how many raises per round.
    public static int getRaises(){ 
        return raises; 
    }
    // incements how many raises per round.
    public static void setRaises(){ 
        raises++; 
    }
    
    // Returns which player is dealer.
    public static int getDealer(){ return dealer; }
    
    // Sets the new dealer/small blind/big blind position
    // and updates GFX coords for the d/sb/bb icons.
    public static void setDealer(){

        if (dealer < 5){
            dealer++;
            sBlind++;
            bBlind++;
            PokerManager.updatePlayerChipCoords(dealer);
        }
        else if (dealer == 5){
            dealer++;
            sBlind++;
            bBlind = 0;
            PokerManager.updatePlayerChipCoords(dealer);            
        }
        else if (dealer == 6){
            dealer++;
            sBlind = 0;
            bBlind++;
            PokerManager.updatePlayerChipCoords(dealer);            
        }
        else if (dealer == 7){
            dealer = 0;
            sBlind++;
            bBlind++;
            PokerManager.updatePlayerChipCoords(dealer);            
        }        
    };

    public static int getPotVal(){
        return pot;
    }
    
    public static String getPotString(){
        return "Pot: $" + Integer.toString(pot);
    }
    
    // Only ever called after start game, so used to reset pot value.
    // Need to keep separate from betsPlease, but should be adapted 
    // to call p.bets() and decide whether to fold or not. Start from left of 
    // bBlind then iterate thru 6 times, but add a seperate p.bets() call for 
    // sBlind which sets the currentBet to blind/2.  Allow bBlind to raise?
    // May need a clone of WISI if folding/removing players.
    public static void blindsPlease(){
        pot = 0;
        whoIsStillIn = PokerManager.accessPlayerList();
        // Subtract Small Blind
        whoIsStillIn[sBlind].subtractBank(blind/2);
        // Subtract Big Blind
        whoIsStillIn[bBlind].subtractBank(blind);
        // Update GFX bank values.
        pot = (blind/2) + blind;
        poker.GFX.GFX_SetScene.setBank();
        poker.GFX.GFX_SetScene.updateMoneyChipIcons(PokerManager.getPhase());
    }
    
    public static void betsPlease(){
                
        // For after the blinds have been taken.
        if (pot == blind + (blind/2)){

            // Maybe replace for i loop with for p in whoIsStillIn
            // Start with cloning WISI so that folds can be deleted from WISI
            // Need to start from left of bBlind though.
            // This would call p.bet();
            for (int i=bBlind+1; i < bBlind+7; i++){
                if (i < 8){
                    // Subtract Big Blind from remaining players & add to pot.
                    whoIsStillIn[i].subtractBank(blind);
                    pot = pot + blind;
                }
                else {
                    // Subtract Big Blind from remaining players & add to pot.
                    whoIsStillIn[i-8].subtractBank(blind);
                    pot = pot + blind;  
                }
            }
            whoIsStillIn[sBlind].subtractBank(blind/2);
            pot = pot + (blind/2);
            savePlayerBankToFile();
        }
        // For later rounds.  Need to track start p# of better?
        // Need to track once all people have called.
        else {
/**
            int currentBet = 0;
            int countBets = 0;
            int expectedBets = whoIsStillIn.length;
            
            while (countBets < expectedBets){
                
                int bet = whoIsStillIn[countBets].bet(currentBet);
                
               // [player fold action] 
                if (bet < 0){
                    // Set player cards to red X cards (reqs method).
                    // Remove player from whoIsStillIn (whoIsStillIn.remove(countBets);)                    
                    expectedBets = whoIsStillIn.length;                
                } 

                // [player check action]
                else if (bet == 0 && currentBet == 0){
                    // nothing?  Update GFX bet text string using GFX.setBank?
                }
                
                // [player call action]
                else if (bet == currentBet && currentBet > 0){                    
                    whoIsStillIn[countBets].subtractBank(bet);
                    Cashier.savePlayerBankToFile();
                    pot = pot + bet;
                // Update p bet string
                // Update GFX bet text string using GFX.setBank?                                        
                } 
                
                // [player raise action]
                else if (bet > currentBet){
                    setRaises();
                    whoIsStillIn[countBets].subtractBank(bet);
                    Cashier.savePlayerBankToFile();
                    pot = pot + bet;                    
                    currentBet = bet;
                // Update GFX text. (saveBank() maybe?)
                // Increment player bet text on screen or in Hand?
                // Need to kick off a round of call betting.
                }
            }
                
            currentBet++;
            countBets++;
 */       
        }

        // Update bank, bet and pot fields in GFX class.
        poker.GFX.GFX_SetScene.setBank();                    
        poker.GFX.GFX_SetScene.updateMoneyChipIcons(PokerManager.getPhase());
    }
    

    
    public static void payWinnings(ArrayList<Hand> winnerList){
        
        int winnings = pot / winnerList.size();
        
        for (Hand P : winnerList){
            P.addBank(winnings);
        }

        savePlayerBankToFile();        
        
        // Update bank and pot fields in GFX class.
        poker.GFX.GFX_SetScene.setBank();
        poker.GFX.GFX_SetScene.updateMoneyChipIcons(PokerManager.getPhase());        
    }
    
    public static void loadAndSetPlayerBankValueFromFile(){

        // For extracting the gameID and bank values from poker.properties file.
        Properties prop = new Properties();
        InputStream input = null;
        OutputStream output = null;

        // Load poker.properties and assign/increment GameID to gameIdLabel
        try {

            String filename = "poker.properties";
                    
            input = PokerManager.class.getClassLoader().getResourceAsStream(filename);

            if(input==null){
    	        System.out.println("Unable to load " + filename + " as input.");
                return;
            }                      

            output = new FileOutputStream(filename);

            // load data props file.
            prop.load(input);

            output = new FileOutputStream("./build/classes/poker.properties");

            // Retrieve player bank values.
            for (int i=0; i<8; i++){
                String newString = "P"+(i+1)+"Bank";
                String bankFromFile = prop.getProperty(newString, "500");
                PokerManager.accessPlayerList()[i].setBankToPlayer(bankFromFile);
                PokerManager.POKER_LOGGER.log(Level.INFO, newString + " = " + bankFromFile);                            
            }    

            // Save properties to txt file in project root folder.
            prop.store(output, null);
        } 
        catch (IOException io) {
            PokerManager.POKER_LOGGER.log(Level.WARNING, "Failed to load data.propeties.  Did not bank.");
            io.printStackTrace();
        } 
        finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    PokerManager.POKER_LOGGER.log(Level.WARNING, "Failed to close data.propeties input stream.");
                    e.printStackTrace();
                }
            } 
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    PokerManager.POKER_LOGGER.log(Level.WARNING, "Failed to close data.propeties output stream.");
                    e.printStackTrace();
                }
            }
        }                                
//                String bankToFile = prop.setProperty(newString, newString)

    }

    // For use after every blind or winnings is applied to player bank.
    public static void savePlayerBankToFile(){

        // For extracting the gameID and bank values from poker.properties file.
        Properties prop = new Properties();
        InputStream input = null;
        OutputStream output = null;

        // Load poker.properties and save player bank field to file.
        try {

            String filename = "poker.properties";
                    
            input = PokerManager.class.getClassLoader().getResourceAsStream(filename);

            if(input==null){
    	        System.out.println("Unable to load " + filename + " as input.");
                return;
            }                      

            output = new FileOutputStream(filename);

            // load data props file.
            prop.load(input);

            output = new FileOutputStream("./build/classes/poker.properties");

            // Retrieve player bank values from Hand objects then save to file.
            for (int i=0; i<8; i++){
                String playerBank = PokerManager.accessPlayerList()[i].getBank();
                String fileBankString = "P"+(i+1)+"Bank";
                prop.setProperty(fileBankString, playerBank);
                PokerManager.POKER_LOGGER.log(Level.INFO, fileBankString + " = " + playerBank);                            
            }    

            // Save properties to txt file in project root folder.
            prop.store(output, null);
        } 
        catch (IOException io) {
            PokerManager.POKER_LOGGER.log(Level.WARNING, "Failed to load data.propeties.  Did not bank.");
            io.printStackTrace();
        } 
        finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    PokerManager.POKER_LOGGER.log(Level.WARNING, "Failed to close data.propeties input stream.");
                    e.printStackTrace();
                }
            } 
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    PokerManager.POKER_LOGGER.log(Level.WARNING, "Failed to close data.propeties output stream.");
                    e.printStackTrace();
                }
            }
        }                                
    }    
         
}