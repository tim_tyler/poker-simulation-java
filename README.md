Poker Simulation (Java)
==========

Simulating a poker game of 8 non-interactive players, the program 'deals' 8 hands and calculates the hand type of each player for each round of a poker game, then declares the winner and assigns the winning to each player's bank.

Requires JDK 7. Paths are all relative so should run OK from any location.

Features:
- Animated 2D graphics! (Java AWT)
- Persistent state saved to an .ini file!
- Logging!
- Background Music!

This was my first self-taught Java program, written during the summer before I started my Software Engineering BSc @ University of Salford.

There is a known issue where the properties file is wiped clean after a Clean & Build operation by an IDE, so there is a backup properties file that can be reinstated after a Clean & Build. Bug Fixes welcome! As is any feedback/criticism as I wrote this before stepping foot in a classroom, and haven't come back to it since ;)